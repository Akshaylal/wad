<%-- 
    Document   : login
    Created on : 2 Jan, 2022, 7:03:16 PM
    Author     : akshaylals
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login</title>
    </head>
    <body>
        <form method="POST" action="LoginServlet">
            <label for="username">Username:</label>
            <input name="username" type="text"><br/>
            <label for="password">Password:</label>
            <input name="password" type="password"><br/>
            <input type="submit" value="Login"><br/>
            <% if(request.getParameter("e") != null) { %>
            <div style="color:red;">Invalid Login Credentials</div>
            <% } %>
        </form>
    </body>
</html>
