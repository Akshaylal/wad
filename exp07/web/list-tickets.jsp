<%-- 
    Document   : list-tickets
    Created on : 8 Jan, 2022, 3:22:53 PM
    Author     : akshaylals
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="com.exp07.beans.TicketBean"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>tickets</title>
    </head>
    <body>
        <%
            ArrayList<TicketBean> tickets = TicketBean.listTickets();
            
            
            if (tickets != null){
                for(int i = 0; i < tickets.size(); i++){
                    out.println("Name : " + tickets.get(i).getName() + "<br/>");
                    out.println("From : " + tickets.get(i).getFrom() + "<br/>");
                    out.println("To : " + tickets.get(i).getTo() + "<br/>");
                    out.println("Price : " + tickets.get(i).getPrice() + "<br/><br/>");
                }
            }else{
                out.println("No tickets");
            }
        %>
    </body>
</html>
