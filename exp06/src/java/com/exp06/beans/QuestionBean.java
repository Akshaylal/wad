/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exp06.beans;

import java.io.*;

/**
 *
 * @author akshaylals
 */
 
 /*
 CREATE TABLE questions(id SERIAL PRIMARY KEY, question VARCHAR(150), option0 VARCHAR(50), option1 VARCHAR(50), option2 VARCHAR(50), option3 VARCHAR(50), answer INTEGER);
INSERT INTO questions(question, option0, option1, option2, option3, answer) VALUES 
    ('On Twitter, what is this sign "#" called?', 'Sharp', 'Hash', 'Hashtag', 'Pound', 2),
    ('Which company marketed the Amiga 1000 personal computer?', 'Intellivision', 'Apple', 'IBM', 'Commodore', 3),
    ('Which car company makes the famous Carrera model?', 'BMW', 'Porsche', 'Tesla', 'Tata', 1),
    ('test2What did Apple ditch on their iPhone 7?', 'USB port', 'Headphone Jack', 'Camera', 'Face id', 1),
    ('What product did Amazon first start out selling?', 'Books', 'Newspaper', 'TV', 'Computers', 0),
    ('Which social media app only lets you view pictures and messages for a limited time?', 'WhatsApp', 'Facebook', 'Snapchat', 'Instagram', 2);
*/

public class QuestionBean implements Serializable {
    private String question;
    private String[] options;
    private int answer;
    
    public void setQuestion(String question){
        this.question = question;
    }
    
    public void setAnswer(int answer){
        this.answer = answer;
    }
    
    public void setOptions(String[] options){
        this.options = options;
    }
    
    
    public String getQuestion(){
        return question;
    }
    
    public int getAnswer(){
        return answer;
    }
    
    public String[] getOptions(){
        return options;
    }
}
