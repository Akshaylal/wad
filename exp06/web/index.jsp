<%-- 
    Document   : index
    Created on : 29 Dec, 2021, 5:12:07 PM
    Author     : akshaylals
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <jsp:useBean id="questionsBean" class="com.exp06.beans.QuestionsBean"></jsp:useBean>
        
        <c:set var="count" value="1" scope="page"/>
        <c:set var="questions" value="${questionsBean.questions}" scope="session"/>
        
        <form action="result.jsp" method="POST">
            <c:forEach var="question" items="${questions}">
                <c:out value="${count}"/>

                <c:out value="${question.question}"/><br/>

                <c:forEach var="i" begin="0" end="3">
                    <input type="radio" name="q${count}" value="${i}">
                    <c:out value="${question.options[i]}"/>
                </c:forEach>
                <c:set var="count" value="${count + 1}" scope="page"/>
                <br/><br/>
            </c:forEach>
            <input type="submit">
        </form>
    </body>
</html>
