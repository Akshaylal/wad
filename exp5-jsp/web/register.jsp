<%-- 
    Document   : register
    Created on : 6 Jan, 2022, 6:38:34 PM
    Author     : akshaylals
--%>

<%@page import="java.sql.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Register</title>
    </head>
    <body>
        <form method="register.jsp" method="POST">
            Username : <input type="text" name="username"><br/>
            Password : <input type="password" name="password"><br/>
            Fullname : <input type="text" name="fullname"><br/>
            Address : <textarea name="address"></textarea><br/>
            Email : <input type="email" name="email"><br/>
            Phone : <input type="number" name="phone"><br/>
            <input type="submit" value="Register"><br/>
        </form>
        
        <% 
            if(request.getParameter("username") != null){
                
                try{
                
                Class.forName("org.postgresql.Driver");
            
                Connection c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/bookshop", "postgres", "root");
            
                PreparedStatement s = c.prepareStatement(
                        "INSERT INTO users(username, password, fullname, address, email, phone) VALUES (?, ?, ?, ?, ?, ?);");
                
                s.setString(1, request.getParameter("username"));
                s.setString(2, request.getParameter("password"));
                s.setString(3, request.getParameter("fullname"));
                s.setString(4, request.getParameter("address"));
                s.setString(5, request.getParameter("email"));
                s.setString(6, request.getParameter("phone"));
                
                s.execute();
                response.sendRedirect("login.jsp");
                
                }catch(Exception e){
                    System.out.println(e);
                    out.println("error registering<br/>");
                }
            }
        %>
    </body>
</html>
