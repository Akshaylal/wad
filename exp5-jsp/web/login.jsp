<%-- 
    Document   : login
    Created on : 6 Jan, 2022, 6:22:47 PM
    Author     : akshaylals
--%>

<%@page import="java.sql.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <form action="login.jsp" method="POST">
            Username: <input type="text" name="username"><br/>
            Password: <input type="password" name="password"><br/>
            <input type="submit" value="Submit"><a href="register.jsp">Register</a>
        </form>
        
        
        <% 
            if(request.getParameter("username") != null){
            String username = request.getParameter("username");
            String password = request.getParameter("password");
            
            Class.forName("org.postgresql.Driver");
            
            Connection c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/bookshop", "postgres", "root");
            
            PreparedStatement s = c.prepareStatement("SELECT * FROM users WHERE username =? AND password=?;");
            s.setString(1, username);
            s.setString(2, password);
            
            ResultSet r = s.executeQuery();
            
            if(r.next()){
                session.setAttribute("user", username);
                response.sendRedirect("index.jsp");
            }else{
                out.println("<br/>Invalid login<br/>");
            }
            }
        %>
    </body>
</html>
