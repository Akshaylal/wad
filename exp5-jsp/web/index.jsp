<%-- 
    Document   : index
    Created on : 6 Jan, 2022, 6:08:26 PM
    Author     : akshaylals
--%>

<%@page import="java.sql.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Bookshop</title>
    </head>
    <body>
        
        <%
            if(session.getAttribute("user") != null){
                out.println("Welcome, " + session.getAttribute("user") + "<br/>");
            }else{
                out.println("<a href=\"login.jsp\">Login</a><br> ");
            }
            
            Class.forName("org.postgresql.Driver");
            
            Connection c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/bookshop", "postgres", "root");
            
            Statement s = c.createStatement();
            
            ResultSet r = s.executeQuery("SELECT * FROM books;");
            
            while(r.next()){
                out.println(r.getString("name") + "<br/>");
                out.println(r.getString("author") + "<br/>");
                out.println(r.getFloat("price") + "<br/><br/>");
            }
        %>
        
        <h1>hello</h1>
    </body>
</html>
