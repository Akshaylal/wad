<%-- 
    Document   : login
    Created on : 22 Nov, 2021, 9:24:47 PM
    Author     : akshaylals
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Login</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <form action="LoginServlet" method="POST">
            <label for="username">Username: </label>
            <input type="text" name="username"><br/>
            <label for="password">Password: </label>
            <input type="password" name="password"><br/>
            <input type="submit" value="Login">
            <a href="register.html">Register</a>
            <br/>
        </form>
        <% if (request.getParameter("e") != null){ %>
        <div style="color: red;">Invalid Login credentials</div>
        <% } %>
    </body>
</html>

