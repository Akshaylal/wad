/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exp02.auth;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author akshaylals
 */
public class RegistrationServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.sendRedirect("register.html");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String fullname = request.getParameter("fullname");
        String address  = request.getParameter("address");
        String email    = request.getParameter("email");
        String phone    = request.getParameter("phone");
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        
                
        try{
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/wad", "postgres", "root");
            Statement s = c.createStatement();
            s.executeUpdate("INSERT INTO users(fullname, address, email, phone, username, password) VALUES ('" + 
                    fullname + "', '" + 
                    address  + "', '" + 
                    email    + "', '" + 
                    phone    + "', '" + 
                    username + "', '" + 
                    password + "');"  );
            
            response.sendRedirect("index.jsp");
        }catch(Exception e){
            System.out.println(e);
        }
        
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Error</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1 style='color:red;'>Error creating user.</h1>");
            out.println("<br><a href='register.html'>Register</a>");
            out.println("</body>");
            out.println("</html>");
        }
    }

}
