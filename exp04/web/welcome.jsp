<%-- 
    Document   : welcome
    Created on : 8 Dec, 2021, 11:27:44 AM
    Author     : akshaylals
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Welcome</title>
    </head>
    <body>
        <jsp:useBean id="user" class="com.exp04.model.LoginBean" />
        <jsp:setProperty name="user" property="*" />
        <h1>Welcome <%= user.getUsername() %> </h1>
    </body>
</html>
