/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exp05.beans;

import java.sql.*;
import java.util.ArrayList;

/**
 *
 * @author akshaylals
 */
/*
CREATE TABLE books(id SERIAL PRIMARY KEY,
    name VARCHAR(50),
    author VARCHAR(20),
    price DECIMAL(10, 2));
INSERT INTO books(name, author, price) VALUES 
    ('Artificial Intelligence and Soft Computing', 'Amit Konar', 695),
    ('The Complete Refrence Java2', 'Herbert Schildt', 700),
    ('Machine Learning', 'Tom M. Mitchell', 500),
    ('Data and Computer Communications', 'William Stallings', 1000),
    ('One Arranged Murder', 'Chetan Bhagat', 225),
    ('400 Days', 'Chetan Bhagat', 250),
    ('The Girl in Room 105', 'Chetan Bhagat', 112),
    ('One Indian Girl', 'Chetan Bhagat', 135),
    ('The White Tiger', 'Aravind Adiga', 231),
    ('The Alchemist', 'Paulo Coelho', 228),
    ('The Archer', 'Paulo Coelho', 228),
    ('The Monk Who Sold His Ferrari', 'Robin Sharma', 170),
    ('Who Will Cry When You Die?', 'Robin Sharma', 140),
    ('The 5 AM Club: Own Your Morning, Elevate Your Life', 'Robin Sharma', 235);
*/
    public class BooksBean implements java.io.Serializable {
    ArrayList<BookBean> books = new ArrayList<>();
    
    public ArrayList<BookBean> getBooks(){
        try{
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager.getConnection(
                    "jdbc:postgresql://localhost:5432/wad", "postgres", "root");
            
            Statement s = c.createStatement();
            ResultSet r = s.executeQuery("SELECT * FROM books;");
            
            while(r.next()){
                BookBean b = new BookBean();
                b.setId(r.getInt("id"));
                b.setName(r.getString("name"));
                b.setAuthor(r.getString("author"));
                b.setPrice(r.getFloat("price"));
                
                books.add(b);
            }
            
            return books;
            
        }catch(Exception e){
            System.out.println(e);
        }
        
        return null;
    }
    
    public ArrayList<BookBean> findBooks(String search){
        try{
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/wad", "postgres", "root");
            PreparedStatement s = c.prepareStatement(
                    "SELECT * FROM books WHERE name iLIKE ? OR author iLIKE ?;");
            
            s.setString(1, "%" + search + "%");
            s.setString(2, "%" + search + "%");
            
            ResultSet r = s.executeQuery();
            
            while(r.next()){
                BookBean b = new BookBean();
                b.setId(r.getInt("id"));
                b.setName(r.getString("name"));
                b.setAuthor(r.getString("author"));
                b.setPrice(r.getFloat("price"));
                
                books.add(b);
            }
            
            return books;
        }catch(Exception e){
            System.out.println(e);
        }
        return null;
    }
    
    public BookBean getBook(int id){
        try{
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager.getConnection(
                    "jdbc:postgresql://localhost:5432/wad", "postgres", "root");
            
            PreparedStatement s = c.prepareStatement("SELECT * FROM books WHERE id=?;");
            
            s.setInt(1, id);
            
            ResultSet r = s.executeQuery();
            
            if(r.next()){
                BookBean b = new BookBean();
                b.setId(r.getInt("id"));
                b.setName(r.getString("name"));
                b.setAuthor(r.getString("author"));
                b.setPrice(r.getFloat("price"));
                
                return b;
            }else{
                return null;
            }
            
        }catch(Exception e){
            System.out.println(e);
        }
        
        return null;
    }
}
