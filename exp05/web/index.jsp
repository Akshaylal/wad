<%-- 
    Document   : index
    Created on : 2 Jan, 2022, 7:02:57 PM
    Author     : akshaylals
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <jsp:useBean id="user" scope="session" class="com.exp05.beans.LoginBean"></jsp:useBean>
        <jsp:useBean id="booksbean" scope="page" class="com.exp05.beans.BooksBean"></jsp:useBean>
        
        <c:choose>
            <c:when test="${user.username != null}">
                Welcome, ${user.username} <a href="LoginServlet?logout">Logout</a>
            </c:when>
            <c:otherwise>
                <a href="login.jsp">Login</a>
            </c:otherwise>
        </c:choose>
                <br/><br/><br/>
                <a href="cart.jsp">Cart</a>
                <br/>
                <form action="index.jsp" method="GET">
                    <input type="text" name="search">
                    <input type="submit" value="Search">
                </form>
        
        <c:choose>
            <c:when test="<%= request.getParameter("search") != null %>">
                <c:set scope="page" var="books" value="<%= booksbean.findBooks(request.getParameter("search")) %>"></c:set>
            </c:when>
            <c:otherwise>
                <c:set scope="page" var="books" value="<%= booksbean.getBooks() %>"></c:set>
            </c:otherwise>
        </c:choose>
        
        
        <c:forEach var="book" items="${books}">
            ${book.name}<br/>
            ${book.author}<br/>
            ${book.price}<br/>
            <a href="CartServlet?add=${book.id}">Add to Cart</a><br/><br/>
        </c:forEach>
        
    </body>
</html>
