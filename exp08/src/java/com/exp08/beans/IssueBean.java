/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exp08.beans;

import java.sql.*;
import java.util.ArrayList;
/**
 *
 * @author akshaylals
 */

// CREATE TABLE issued (id SERIAL PRIMARY KEY, bookId INT, issueDate VARCHAR(10), returnDate VARCHAR(10), name VARCHAR(10));
public class IssueBean implements java.io.Serializable {
    private int id;
    private int bookId;
    private String issueDate;
    private String returnDate;
    private String name;

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the bookId
     */
    public int getBookId() {
        return bookId;
    }

    /**
     * @param bookId the bookId to set
     */
    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the issueDate
     */
    public String getIssueDate() {
        return issueDate;
    }

    /**
     * @param issueDate the issueDate to set
     */
    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }

    /**
     * @return the returnDate
     */
    public String getReturnDate() {
        return returnDate;
    }

    /**
     * @param returnDate the returnDate to set
     */
    public void setReturnDate(String returnDate) {
        this.returnDate = returnDate;
    }
    
    
    public static ArrayList<IssueBean> listIssued(){
        ArrayList<IssueBean> issued = new ArrayList<>();
        
        try{
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/wad", "postgres", "root");
            
            Statement s = c.createStatement();
            
            ResultSet r = s.executeQuery("SELECT * FROM issued;");
            
            while (r.next()){
                IssueBean t = new IssueBean();
                
                t.setId(r.getInt("id"));
                t.setName(r.getString("name"));
                t.setBookId(r.getInt("bookId"));
                t.setIssueDate(r.getString("issueDate"));
                t.setReturnDate(r.getString("returnDate"));
                
                issued.add(t);
            }
        }catch(Exception e){
            System.out.println(e);
        }
        
        return issued;
    }
    
    public boolean issueBook(){
        try{
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/wad", "postgres", "root");
            
            PreparedStatement s = c.prepareStatement("INSERT INTO issued(bookId, name, issueDate, returnDate) VALUES (?, ?, ?, ?);");
            
            s.setInt(1, bookId);
            s.setString(2, name);
            s.setString(3, issueDate);
            s.setString(4, returnDate);
                        
            s.executeUpdate();
            return true;
        }catch(Exception e){
            System.out.println(e);
        }
        return false;
    }
    
    public static boolean returnBook(int id){
        try{
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/wad", "postgres", "root");
            
            PreparedStatement s = c.prepareStatement("DELETE FROM issued WHERE id=?");
            
            s.setInt(1, id);
                        
            s.executeUpdate();
            return true;
        }catch(Exception e){
            System.out.println(e);
        }
        return false;
    }
}
