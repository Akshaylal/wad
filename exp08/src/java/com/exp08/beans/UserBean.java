/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exp08.beans;

import java.io.*;
import java.sql.*;


/**
 *
 * @author akshaylals
 */
public class UserBean implements Serializable{
    private String username, password, fullname, address, email, phone;
    
    public void setUsername(String username){
        this.username = username;
    }
    
    public void setPassword(String password){
        this.password = password;
    }
    
    public void setFullname(String fullname){
        this.fullname = fullname;
    }
    
    public void setAddress(String address){
        this.address = address;
    }
    
    public void setEmail(String email){
        this.email = email;
    }
    
    public void setPhone(String phone){
        this.phone = phone;
    }
    
    
    public String getUsername(){
        return username;
    }
    
    public String getPassword(){
        return password;
    }
    
    public String getFullname(){
        return fullname;
    }
    
    public String getAddress(){
        return address;
    }
    
    public String getEmail(){
        return email;
    }
    
    public String getPhone(){
        return phone;
    }
    
    
    public boolean login(){
        try{
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/wad", "postgres", "root");
            
            PreparedStatement s = c.prepareStatement("SELECT * FROM users WHERE username=? AND password=?;");
            
            s.setString(1, username);
            s.setString(2, password);
            
            ResultSet r = s.executeQuery();
            
            if (r.next()){
                fullname = r.getString("fullname");
                address  = r.getString("address");
                email    = r.getString("email");
                phone    = r.getString("phone");
                return true;
            }else{
                return false;
            }
        }catch(Exception e){
            System.out.println(e);
        }
        return false;
    }
}
