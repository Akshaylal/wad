/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exp08.beans;

import java.io.*;
import java.sql.*;
import java.util.*;
/**
 *
 * @author akshaylals
 */
public class BookBean implements Serializable {
    private int id;
    private String name, author;

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the author
     */
    public String getAuthor() {
        return author;
    }

    /**
     * @param author the author to set
     */
    public void setAuthor(String author) {
        this.author = author;
    }
    
    
    public static ArrayList<BookBean> listBooks(){
        ArrayList<BookBean> books = new ArrayList<>();
        
        try{
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/wad", "postgres", "root");
            
            Statement s = c.createStatement();
            
            ResultSet r = s.executeQuery("SELECT * FROM books;");
            
            while (r.next()){
                BookBean book = new BookBean();
                
                book.setId(r.getInt("id"));
                book.setName(r.getString("name"));
                book.setAuthor(r.getString("author"));
                
                books.add(book);
            }
        }catch(Exception e){
            System.out.println(e);
        }
        
        return books;
    }
    
    public static BookBean getBook(int id){
        try{
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/wad", "postgres", "root");
            
            PreparedStatement s = c.prepareStatement("SELECT * FROM books WHERE id=?;");
            
            s.setInt(1, id);
            
            ResultSet r = s.executeQuery();
            
            if(r.next()){
                BookBean book = new BookBean();
                
                book.setId(r.getInt("id"));
                book.setName(r.getString("name"));
                book.setAuthor(r.getString("author"));
                
                return book;
            }
        }catch(Exception e){
            System.out.println(e);
        }
        
        return null;
    }
}
