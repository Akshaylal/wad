/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exp08.servlet;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;


import com.exp08.beans.UserBean;

/**
 *
 * @author akshaylals
 */
public class LoginServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if(request.getParameter("logout") != null){
            HttpSession session = request.getSession();
            session.invalidate();
        }
        response.sendRedirect("index.jsp");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        UserBean bean = new UserBean();
        
        bean.setUsername(request.getParameter("username"));
        bean.setPassword(request.getParameter("password"));
        
        if(bean.login()){
            HttpSession session = request.getSession();
            bean.setPassword("");
            session.setAttribute("user", bean.getFullname());
            response.sendRedirect("list-books.jsp");
        }else{
            response.sendRedirect("index.jsp?e");
        }
    }
}