<%-- 
    Document   : return-book
    Created on : 10 Jan, 2022, 7:44:19 AM
    Author     : akshaylals
--%>

<%@page import="com.exp08.beans.IssueBean"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Return Book</title>
    </head>
    <body>
        <% if(session.getAttribute("user") == null) response.sendRedirect("index.jsp"); %>
        <% 
            if(request.getParameter("id") != null){
                IssueBean.returnBook(Integer.parseInt(request.getParameter("id")));
                response.sendRedirect("return-book.jsp");
            }
        %>
        
        <a href="list-books.jsp">Issue Book</a><br/>
        Welcome, <%= session.getAttribute("user") %>
        <a href="LoginServlet?logout">Logout</a><br/><br/>
                
        <%
            ArrayList<IssueBean> issued = IssueBean.listIssued();
            
            if (issued.size() > 0){
        %>
            <table>
                <tr>
                    <th>Id</th>
                    <th>Book Id</th>
                    <th>Name</th>
                    <th>Issue Date</th>
                    <th>Return Date</th>
                    <th>Action</th>
                </tr>
                
                <% for(int i = 0; i < issued.size(); i++){ %>
                <tr>
                    <td><%= issued.get(i).getId() %></td>
                    <td><%= issued.get(i).getBookId() %></td>
                    <td><%= issued.get(i).getName() %></td>
                    <td><%= issued.get(i).getIssueDate() %></td>
                    <td><%= issued.get(i).getReturnDate() %></td>
                    <td><a href="return-book.jsp?id=<%= issued.get(i).getId() %>">Return</a></td>
                </tr>
                <% } %>
            </table>
                    
            <%
                }else{
                    out.println("No Books issued");
                }
            %>
        </div>
    </body>
</html>
