<%-- 
    Document   : login
    Created on : 2 Jan, 2022, 7:03:16 PM
    Author     : akshaylals
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login</title>
    </head>
    <body>
        <% if(session.getAttribute("user") != null) response.sendRedirect("list-books.jsp"); %>
        
        <form action="LoginServlet" method="POST">
            Username: <input type="text" name="username"><br/>
            Password: <input type="password" name="password"><br/>
            <input type="submit" value="Login"><br/>
            <% if (request.getParameter("e") != null){ %>
                <div style="color: red;">Invalid Login credentials</div>
            <% } %>
        </form>
    </body>
</html>
