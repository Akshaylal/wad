<%-- 
    Document   : issue-book
    Created on : 10 Jan, 2022, 7:44:05 AM
    Author     : akshaylals
--%>

<%@page import="java.sql.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.exp08.beans.IssueBean"%>
<%@page import="com.exp08.beans.BookBean"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Issue Book</title>
    </head>
    <body>
        <% if(session.getAttribute("user") == null) response.sendRedirect("index.jsp"); %>
        
        <a href="return-book.jsp">Return Book</a><br/>
        Welcome, <%= session.getAttribute("user") %>
        <a href="LoginServlet?logout">Logout</a>
        
        <form action="issue-book.jsp" method="POST">
            <input name="id" type="hidden" value="<%= request.getParameter("id") %>">
            Book Name: <input type="text" disabled="true" value="<%= BookBean.getBook(Integer.parseInt(request.getParameter("id"))).getName() %>"><br/>
            Name: <input type="text" name="name"><br/>
            Issue Date: <input type="date" name="issueDate"><br/>
            Return Date: <input type="date" name="returnDate"><br/>
            <input type="submit" value="Issue">
        </form>
            
        <%
            if(request.getParameter("name") != null){
                IssueBean issue = new IssueBean();               
                
                issue.setBookId(Integer.parseInt(request.getParameter("id")));
                issue.setName(request.getParameter("name"));
                issue.setIssueDate(request.getParameter("issueDate"));
                issue.setReturnDate(request.getParameter("returnDate"));
                
                
                if(issue.issueBook()){
                    out.println("Book issued successfully");
                }else{
                    out.println("Failed to issue book");
                }
            }
            
        %>
        </div>
    </body>
</html>
