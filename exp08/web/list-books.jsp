<%-- 
    Document   : list-books
    Created on : 10 Jan, 2022, 10:55:24 AM
    Author     : akshaylals
--%>

<%@page import="com.exp08.beans.BookBean"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Issue Book</title>
    </head>
    <body>
        <% if(session.getAttribute("user") == null) response.sendRedirect("index.jsp"); %>
        <a href="return-book.jsp">Return Book</a><br/>
        Welcome, <%= session.getAttribute("user") %><a href="LoginServlet?logout">Logout</a><br/><br/>
        
        <% 
            ArrayList<BookBean> books = BookBean.listBooks();
            
            if(books.size() > 0){
                for(int i = 0; i < books.size(); ++i){
                    out.println(books.get(i).getName() + "<br/>");
                    out.println(books.get(i).getAuthor() + "<br/>");
                    out.println("<a href=\"issue-book.jsp?id=" + books.get(i).getId() + "\">Issue</a><br/><br/>");
                }
            }else{
                out.println("No Books found");
            }
        %>
    </body>
</html>
