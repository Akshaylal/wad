class Phone{
    String os;

    public Phone(){
        this.os = "Unknown";
    }
    
    public void getOs() {
        System.out.println(os);
    }

    public void call(){
        System.out.println("Calling...");
    }
}


class Mi extends Phone{
    public Mi(){
        this.os = "Android";
    }
}


public class Inheritance {

    public static void main(String[] args) {
        Phone p = new Phone();
        p.call();
        p.getOs();
        Mi m = new Mi();
        m.call();
        m.getOs();
    }
}