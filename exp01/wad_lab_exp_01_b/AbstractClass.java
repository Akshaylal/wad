abstract class Vehicle{
    public abstract void wheels();

    public void accelerate() {
        System.out.println("Go Forward");
    }

    public void brake(){
        System.out.println("Stop!");
    }
}


class Car extends Vehicle {
    public void wheels(){
        System.out.println("I have 4 wheels!");
    }
}


class Bike extends Vehicle {
    public void wheels(){
        System.out.println("I have 2 wheels!");
    }
}


public class AbstractClass {

    public static void main(String[] args) {
        Car c = new Car();
        Bike b = new Bike();

        c.accelerate();
        c.brake();
        c.wheels();
        b.accelerate();
        b.brake();
        b.wheels();

    }
}