interface Animal {
    public void move();
    public void sound();
}


class Cat implements Animal{
    public void move() {
        System.out.println("Walk");
    }
    public void sound(){
        System.out.println("Meow");
    }
}


public class Interface {
    public static void main(String[] args) {
        Cat c = new Cat();
        c.move();
        c.sound();
    }
}
