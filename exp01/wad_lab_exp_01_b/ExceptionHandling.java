public class ExceptionHandling {
    public static void main(String[] args) {
        int[] num = { 1, 2, 3, 4, 5 };
        int sum = 0, i = 0;
        
        try {
            while (true) {
                sum += num[i++];
            }
            
        } catch (Exception e) {
            System.out.println("out of range");
            System.out.println(sum);
            System.out.println(e);
        }
    }
}
