public class UserClass {
    String name;
    int id;

    
    public UserClass(int id, String name) {
        this.name = name;
        this.id = id;
    }


    public void printDetails(){
        System.out.printf("ID: %d\tName: %s\n", id, name);
    }


    public static void main(String[] args) {
        UserClass u1 = new UserClass(1, "test1");
        u1.printDetails();
    }
    
}